<?php
require "config.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Data-Type');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    exit;
}

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER["REQUEST_URI"];

switch ($uri) {
    case "/paths":
        paths();
        break;
    case "/paths/add":
        add();
        break;
    default:
        pageNotFound();
}

function pageNotFound()
{
    http_response_code(404);
    echo "Page Not Found";
    exit();
}

function add() {
    header('Allow: POST');
    header('Content-Type: application/json');

    $jsonData = file_get_contents("php://input");
    $data = json_decode($jsonData, true);
    if (!empty($data)) {
        $db = Database::connect();

        $path = $data["path"];

        date_default_timezone_set('Europe/Paris');
        $date = date('Y-m-d H:i:s');

        $strokeColor = $data["strokeColor"];
        $lineWidth = $data["lineWidth"];

        $statement = $db->prepare('INSERT INTO draws (created_at, stroke_color, line_width)
    VALUES (:created_at, :stroke_color, :line_width)');
        $statement->execute([
            "created_at"      =>    $date,
            "stroke_color"    =>    $strokeColor,
            "line_width"      =>    $lineWidth,
        ]);
        $drawId = $db->lastInsertId();

        $statement = $db->prepare('INSERT INTO draws_path (draw_id, path) VALUES (:draw_id, :path)');
        $statement->execute([
            "draw_id"   =>   $drawId,
            "path"      =>   serialize($path),
        ]);
        $db = Database::disconnect();

        echo json_encode("ok");
    } else {
        echo json_encode("vide");
    }
}

function paths() {
    header('Allow: GET');
    header('Content-Type: application/json');

    $db = Database::connect();

    $statement = $db->query('SELECT draws.id, created_at, stroke_color, line_width, path from draws 
    JOIN draws_path dp on draws.id = dp.draw_id');
    $paths = $statement->fetchAll(PDO::FETCH_ASSOC);

    foreach ($paths as $path) {
        $arr[] = array(
            "id" => intval($path["id"]),
            "createdAt" => $path["created_at"],
            "path" => unserialize($path["path"]),
            "strokeColor" => $path["stroke_color"],
            "lineWidth" => intval($path["line_width"]),
        );
    }
    echo json_encode($arr);
}
